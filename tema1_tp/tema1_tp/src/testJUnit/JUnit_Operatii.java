package testJUnit;

import model.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class JUnit_Operatii {

	@Test
	public void testAdunare() {
		AdunarePolinom adunarePolinom = new AdunarePolinom();

		Polinom primulPolinom = new Polinom();
		primulPolinom.getSirMonoame().add(new Monom(2, 2));
		primulPolinom.getSirMonoame().add(new Monom(1, 3));

		Polinom alDoileaPolinom = new Polinom();
		alDoileaPolinom.getSirMonoame().add(new Monom(1, 1));
		alDoileaPolinom.getSirMonoame().add(new Monom(0, 1));

		Polinom rezultat = adunarePolinom.adunare(primulPolinom, alDoileaPolinom);

		rezultat.verifica();
		rezultat.stergeZero();
		rezultat.ordonareDupaGrad();

		Polinom rezultatCorect = new Polinom();
		rezultatCorect.getSirMonoame().add(new Monom(2, 2));
		rezultatCorect.getSirMonoame().add(new Monom(1, 4));
		rezultatCorect.getSirMonoame().add(new Monom(0, 1));

		assertTrue(rezultatCorect.equals(rezultat));

		rezultatCorect.getSirMonoame().remove(0);

		assertFalse(rezultatCorect.equals(rezultat));
	}

	@Test
	public void testScadere() {
		ScaderePolinom scaderePolinom = new ScaderePolinom();

		Polinom primulPolinom = new Polinom();
		primulPolinom.getSirMonoame().add(new Monom(2, 2));
		primulPolinom.getSirMonoame().add(new Monom(1, 3));

		Polinom alDoileaPolinom = new Polinom();
		alDoileaPolinom.getSirMonoame().add(new Monom(1, 1));
		alDoileaPolinom.getSirMonoame().add(new Monom(0, 1));

		Polinom rezultat = scaderePolinom.scadere(primulPolinom, alDoileaPolinom);

		rezultat.verifica();
		rezultat.stergeZero();
		rezultat.ordonareDupaGrad();

		Polinom rezultatCorect = new Polinom();
		rezultatCorect.getSirMonoame().add(new Monom(2, 2));
		rezultatCorect.getSirMonoame().add(new Monom(1, 2));
		rezultatCorect.getSirMonoame().add(new Monom(0, -1));

		assertTrue(rezultatCorect.equals(rezultat));

		rezultatCorect.getSirMonoame().remove(0);

		assertFalse(rezultatCorect.equals(rezultat));
	}

	@Test
	public void testInmultire() {
		InmultirePolinom inmultirePolinom = new InmultirePolinom();
		
		Polinom primulPolinom = new Polinom();//2x^2+3x
		primulPolinom.getSirMonoame().add(new Monom(2,2));
		primulPolinom.getSirMonoame().add(new Monom(1,3));
		
		Polinom alDoileaPolinom = new Polinom();
		alDoileaPolinom.getSirMonoame().add(new Monom(1,1));
		alDoileaPolinom.getSirMonoame().add(new Monom(0,1));
		
		Polinom rezultat = inmultirePolinom.inmultire(primulPolinom, alDoileaPolinom);
		
		rezultat.verifica();
		rezultat.stergeZero();
		rezultat.ordonareDupaGrad();
		
		Polinom rezultatCorect = new Polinom();
		rezultatCorect.getSirMonoame().add(new Monom(3,2));
		rezultatCorect.getSirMonoame().add(new Monom(2,4));
		rezultatCorect.getSirMonoame().add(new Monom(1,3));
		
		assertTrue(rezultatCorect.equals(rezultat));
		
		rezultatCorect.getSirMonoame().remove(0);
		
		assertFalse(rezultatCorect.equals(rezultat));
	}
	
	@Test
	
	public void testImpartire() {
		ImpartirePolinom impartirePolinom = new ImpartirePolinom();
		
		Polinom primulPolinom = new Polinom(); //x^2+2x+1
		primulPolinom.getSirMonoame().add(new Monom(2,1));
		primulPolinom.getSirMonoame().add(new Monom(1,2));
		primulPolinom.getSirMonoame().add(new Monom(0,1));
		
		Polinom alDoileaPolinom = new Polinom(); //x+1
		alDoileaPolinom.getSirMonoame().add(new Monom(1,1));
		alDoileaPolinom.getSirMonoame().add(new Monom(0,1));
		
		Polinom[] rezultat = null;
		
		try {
			rezultat = impartirePolinom.impartire(primulPolinom, alDoileaPolinom);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
		 }
		
		if(rezultat.length!=2) {
			assertTrue(false);
		}
		
		Polinom rezultatCorect = new Polinom();
		rezultatCorect.getSirMonoame().add(new Monom(1,1));
		rezultatCorect.getSirMonoame().add(new Monom(0,1));
		
		//Polinom rest = new Polinom();
		
		assertTrue(rezultatCorect.equals(rezultat[0]));
	
	}
	
@Test
	
	public void testDerivare() {
		DerivarePolinom derivarePolinom = new DerivarePolinom();
		
		Polinom polinom = new Polinom();
			polinom.getSirMonoame().add(new Monom(3,1));
			polinom.getSirMonoame().add(new Monom(2,2));
			
			Polinom rezultat = derivarePolinom.derivare(polinom);
			
			rezultat.verifica();
			rezultat.stergeZero();
			rezultat.ordonareDupaGrad();
			
			Polinom rezultatCorect = new Polinom();
			rezultatCorect.getSirMonoame().add(new Monom(2,3));
			rezultatCorect.getSirMonoame().add(new Monom(1,4));

			assertTrue(rezultatCorect.equals(rezultat));

	        rezultatCorect.getSirMonoame().remove(0);

	        assertFalse(rezultatCorect.equals(rezultat));
}

@Test

public void testIntegrare() {
	IntegrarePolinom integrarePolinom = new IntegrarePolinom();
	
	Polinom polinom = new Polinom();
		polinom.getSirMonoame().add(new Monom(2,3));
		polinom.getSirMonoame().add(new Monom(1,2));
		polinom.getSirMonoame().add(new Monom(0,1));
		
		Polinom rezultat = integrarePolinom.integrare(polinom);
		
		rezultat.verifica();
		rezultat.stergeZero();
		rezultat.ordonareDupaGrad();
		
		Polinom rezultatCorect = new Polinom();
		rezultatCorect.getSirMonoame().add(new Monom(3,1));
		rezultatCorect.getSirMonoame().add(new Monom(2,1));
		rezultatCorect.getSirMonoame().add(new Monom(1,1));

		assertTrue(rezultatCorect.equals(rezultat));

        rezultatCorect.getSirMonoame().remove(0);

        assertFalse(rezultatCorect.equals(rezultat));
}
}
