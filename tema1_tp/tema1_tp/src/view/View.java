package view;

import java.awt.Color;
import javax.swing.*;

import control.ControlButoane;

public class View {
    private JFrame frame = new JFrame("Calculator Polinoame");
    private JPanel panou;

    private JLabel polinom1;
    private JTextField scriePolinom1;

    private JLabel polinom2;
    private JTextField scriePolinom2;

    private String[] tipuriOperatii = {"Adunare", "Scadere", "Inmultire", "Impartire", "Derivare", "Integrare"};

    private JLabel tipulOperatiei;
    private JComboBox selectare;
    private JLabel rezultat;
    private JTextField scrieRezultat;

    private JButton calculeaza;

    private ControlButoane control;

    public View() {
        super();
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setSize(1000, 500);
        // frame.setResizable(false);

        panou = new JPanel();
        panou.setBackground(Color.CYAN);
        panou.setLayout(null);
        panou.setBounds(0, 0, 900, 500);

        polinom1 = new JLabel("Polinom1:");
        polinom1.setBounds(20, 60, 250, 30);
        scriePolinom1 = new JTextField("Scrie primul polinom");
        scriePolinom1.setBounds(80, 60, 250, 30);

        polinom2 = new JLabel("Polinom2:");
        polinom2.setBounds(640, 60, 250, 30);
        scriePolinom2 = new JTextField("Scrie polinomul al doilea");
        scriePolinom2.setBounds(700, 60, 250, 30);

        tipulOperatiei = new JLabel("Operatie:");
        tipulOperatiei.setBounds(380, 60, 250, 30);
        selectare = new JComboBox(tipuriOperatii);
        selectare.setBounds(450, 60, 125, 30);

        rezultat = new JLabel("Rezultat:");
        rezultat.setBounds(310, 300, 250, 30);
        scrieRezultat = new JTextField();
        scrieRezultat.setBounds(400, 300, 250, 30);

        calculeaza = new JButton("Calculeaza");
        calculeaza.setBounds(400, 250, 250, 30);

        control = new ControlButoane(this);
        calculeaza.addActionListener(control);

        panou.add(polinom1);
        panou.add(scriePolinom1);
        panou.add(polinom2);
        panou.add(scriePolinom2);
        panou.add(tipulOperatiei);
        panou.add(selectare);
        panou.add(rezultat);
        panou.add(scrieRezultat);
        panou.add(calculeaza);

        frame.add(panou);
        frame.setVisible(true);

    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanou() {
        return panou;
    }

    public void setPanou(JPanel panou) {
        this.panou = panou;
    }

    public JLabel getPolinom1() {
        return polinom1;
    }

    public void setPolinom1(JLabel polinom1) {
        this.polinom1 = polinom1;
    }

    public JTextField getScriePolinom1() {
        return scriePolinom1;
    }

    public void setScriePolinom1(JTextField scriePolinom1) {
        this.scriePolinom1 = scriePolinom1;
    }

    public JLabel getPolinom2() {
        return polinom2;
    }

    public void setPolinom2(JLabel polinom2) {
        this.polinom2 = polinom2;
    }

    public JTextField getScriePolinom2() {
        return scriePolinom2;
    }

    public void setScriePolinom2(JTextField scriePolinom2) {
        this.scriePolinom2 = scriePolinom2;
    }

    public String[] getTipuriOperatii() {
        return tipuriOperatii;
    }

    public void setTipuriOperatii(String[] tipuriOperatii) {
        this.tipuriOperatii = tipuriOperatii;
    }

    public JLabel getTipulOperatiei() {
        return tipulOperatiei;
    }

    public void setTipulOperatiei(JLabel tipulOperatiei) {
        this.tipulOperatiei = tipulOperatiei;
    }

    public JComboBox getSelectare() {
        return selectare;
    }

    public void setSelectare(JComboBox selectare) {
        this.selectare = selectare;
    }

    public JLabel getRezultat() {
        return rezultat;
    }

    public void setRezultat(JLabel rezultat) {
        this.rezultat = rezultat;
    }

    public JTextField getScrieRezultat() {
        return scrieRezultat;
    }

    public void setScrieRezultat(JTextField scrieRezultat) {
        this.scrieRezultat = scrieRezultat;
    }

    public JButton getCalculeaza() {
        return calculeaza;
    }

    public void setCalculeaza(JButton calculeaza) {
        this.calculeaza = calculeaza;
    }

}
