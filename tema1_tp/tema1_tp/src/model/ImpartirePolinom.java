package model;

import java.util.ArrayList;
import java.util.List;

/**
 * class ImpartirePolinom
 */
public class ImpartirePolinom {

	/**
	 * Se ocupa de impartirea a doua polinoame
	 * Rezultat este un array, pe pozitia zero este rezultatul,
	 * Pe pozitia 1 este restul
	 * @param primulPolinom
	 * @param alDoileaPolinom
	 * @return
	 * @throws Exception
	 */
	public Polinom[] impartire(Polinom primulPolinom, Polinom alDoileaPolinom) throws Exception{

			
		ScaderePolinom scaderePolinom = new ScaderePolinom();
		
		Polinom[] rezultat = new Polinom[2];
		
		if(alDoileaPolinom.getSirMonoame().isEmpty()) {
			rezultat[0] = primulPolinom;
			rezultat[1] = new Polinom();
			return rezultat;
		}
		primulPolinom.ordonareDupaGrad();
		alDoileaPolinom.ordonareDupaGrad();
		
		primulPolinom.stergeZero();
		primulPolinom.verifica();
		alDoileaPolinom.verifica();
		
		if(alDoileaPolinom.getSirMonoame().indexOf(0)==0)  {
		
			Polinom polinom = new Polinom();
			List<Monom> monomList = new ArrayList();
			for(Monom m:primulPolinom.getSirMonoame()) {
				Monom monom=new Monom(m.getGrad(),m.getCoeficient()/(alDoileaPolinom.getSirMonoame().get(0).getCoeficient()));
				monomList.add(monom);
			}
			
			polinom.setSirMonoame(monomList);
			
			rezultat[0]=polinom;
			rezultat[1]=new Polinom();
			
			return rezultat;
		}
		
		int repeat = primulPolinom.getSirMonoame().size();
		Polinom rezultatImpartire = new Polinom();
		
		int i;
		for(i=0;i<repeat;i++) {
			if(primulPolinom.getSirMonoame().isEmpty()) {
				break;
			}
			int grad =grad = primulPolinom.getSirMonoame().get(0).getGrad()- alDoileaPolinom.getSirMonoame().get(0).getGrad(); 
			double coeficient = primulPolinom.getSirMonoame().get(0).getCoeficient() / alDoileaPolinom.getSirMonoame().get(0).getCoeficient();
				
		Monom monom = new Monom(grad,coeficient);
		Polinom p = new Polinom();
			
		rezultatImpartire.getSirMonoame().add(monom);
			
		//inmultire
		
		for(Monom m : alDoileaPolinom.getSirMonoame()) {
			Monom mon=new Monom(m.getGrad() + monom.getGrad(), m.getCoeficient()*monom.getCoeficient());
			p.getSirMonoame().add(mon);
		}
		p.verifica();
		p.ordonareDupaGrad();
		p.stergeZero();
		
		primulPolinom = scaderePolinom.scadere(primulPolinom, p);
		primulPolinom.stergeZero();
		primulPolinom.ordonareDupaGrad();
		primulPolinom.verifica();
		}
		
		rezultatImpartire.verifica();
		rezultatImpartire.ordonareDupaGrad();
		
		rezultat[0]=rezultatImpartire;
		rezultat[1]=primulPolinom;
		return rezultat;

	
			}
			}	


