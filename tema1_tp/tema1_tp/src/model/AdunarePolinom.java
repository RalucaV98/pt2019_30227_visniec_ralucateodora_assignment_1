package model;

import java.util.ArrayList;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * class AdunarePolinom
 * Se ocupa de adunarea a doua polinoame
 * Rezultat este un polinom
 */
public class AdunarePolinom {

	/**
	 * @param primulPolinom
	 * @param alDoileaPolinom
	 * @return
	 */
	public Polinom adunare(Polinom primulPolinom, Polinom alDoileaPolinom) {
		Polinom rezultat = new Polinom();
		int ok;
		for (Monom i : primulPolinom.getSirMonoame()) {
			ok = 0;
			for (Monom j : alDoileaPolinom.getSirMonoame()) {
				if (i.getGrad() == j.getGrad()) {
					Monom m = new Monom(i.getGrad(),(int) (j.getCoeficient() + i.getCoeficient()));
					rezultat.getSirMonoame().add(m);
					ok = 1;
					break;
				}
			}
			if (ok == 0)
				
				rezultat.getSirMonoame().add(i);
		}
		for (Monom i : alDoileaPolinom.getSirMonoame()) {
			if (!rezultat.contineGrad(i.getGrad()))
				rezultat.getSirMonoame().add(i);
		}

		return rezultat;
	}

}
