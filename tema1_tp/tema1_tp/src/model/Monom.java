package model;

import java.text.DecimalFormat;

/**
 * Class Monom
 */
public class Monom implements Comparable<Monom> {

    private int grad;
    private double coeficient;

    public Monom(int grad, double coeficient) {
        this.grad = grad;
        this.coeficient = coeficient;
    }

    public int getGrad() {
        return grad;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

    public double getCoeficient() {
        return coeficient;
    }

    public void setCoeficient(double coeficient) {
        this.coeficient = coeficient;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#0.00"); //df2.format(input)
        String coef = new String();
        if (coeficient == (double) Math.round(coeficient)) {
            coef = "" + Math.round(coeficient);
        } else {
            coef = df.format(coeficient);
        }
        if (this.coeficient != 0) {
            if (this.grad == 0) // pt a da coef in locul monoamelor cu puterea 0
                //return String.valueOf(coeficient);
                return coef;
            else {
                if (grad != 1) {
                    if (coeficient == 1) {
                        return "X^" + grad;
                    } else {
                        return coef + "*X^" + grad;
                    }
                } else {
                    if (coeficient == 1) {
                        return "X";
                    } else {
                        return coef + "*X";
                    }
                }
            }

        } else {
            return "";
        }

    }
    
    public Monom adunareMonoame(Monom primulMonom, Monom alDoileaMonom) {
		Monom rezultat = new Monom(primulMonom.grad, primulMonom.coeficient + alDoileaMonom.coeficient);
		return rezultat;

	}


    @Override

    public int compareTo(Monom monom) {
        if (monom.grad == this.grad) {
            if (coeficient == monom.coeficient) {
                return 0;
            } else if (coeficient > monom.coeficient) {
                return 1;
            } else {
                return -1;
            }

        } else {
            if (this.grad > monom.grad) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
