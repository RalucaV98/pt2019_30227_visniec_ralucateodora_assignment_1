package model;

public class Parsare {


    /**
     * Proceseaza string-ul si in converteste intr-un polinom
     * @param polinom
     * @return
     * @throws Exception
     */
    public Polinom parsare(String polinom) throws Exception {

        int i;
        String retineCoef = new String();
        String retineGrad = new String();
        Polinom polinomRezultat = new Polinom();
        double creareCoef;
        int creareGrad;
        String[] parts = polinom.split(" ");
        String polinomAfterSplit = new String();

        //length() - imi zice lungimea unui string
        //length - lungimea vectorului
        for (i = 0; i < parts.length; i++) {

            polinomAfterSplit += parts[i];
        }

        polinom = polinomAfterSplit; //avem polinomul fara spatii

        for (i = 0; i < polinom.length(); i++) {
            //polinom[i] - pt a folosi var asta, tb convertit intai intr un sir de caractere
            if ((polinom.charAt(i) == '+' || polinom.charAt(i) == '-') && (polinom.charAt(i + 1) == '+' || polinom.charAt(i + 1) == '-')) //fct pt cele4 cazuri
            {
                switch (polinom.charAt(i)) {
                    case '+':
                        switch (polinom.charAt(i + 1)) {
                            case '+':
                                polinom = polinom.replace("++", "+");
                                break;

                            case '-':
                                polinom = polinom.replace("+-", "-");
                                break;

                        }

                    case '-':
                        switch (polinom.charAt(i + 1)) {
                            case '+':
                                polinom = polinom.replace("-+", "-");
                                break;

                            case '-':
                                polinom = polinom.replace("--", "-");
                                break;
                        }

                }
            }
        }

        int ok = 0;
        int ok2 = 0;

        for (i = 0; i < polinom.length(); i++) {
            if ((polinom.charAt(i) < '0' || polinom.charAt(i) > '9') && polinom.charAt(i) != 'X' && polinom.charAt(i) != 'x' &&
                    polinom.charAt(i) != '+' && polinom.charAt(i) != '-' && polinom.charAt(i) != '^')
                throw new Exception("eroare");
            retineCoef = new String();
            retineGrad = new String();
            ok = 0;
            ok2 = 0;
            if (polinom.charAt(i) == '-') {
                retineCoef += polinom.charAt(i);
                i++;
            }
            while (i < polinom.length() && polinom.charAt(i) >= '0' && polinom.charAt(i) <= '9') //ce caracter e pe indexul i
            {
                retineCoef += polinom.charAt(i);
                i++;
            }
            if (i < polinom.length() && (polinom.charAt(i) == 'x' || polinom.charAt(i) == 'X')) {
                ok = 1;//ne verifica daca avem x
                i++;
            }
            //suntem pe caracterul 'x' la fin while ului
            if (i < polinom.length() && polinom.charAt(i) == '^') //daca nu e scrisa puterea, sa nu sarim peste x si putere
            {
                i++; //am ajuns la grad
                ok2 = 1;
                while (i < polinom.length() && polinom.charAt(i) >= '0' && polinom.charAt(i) <= '9') {
                    retineGrad += polinom.charAt(i);
                    i++;

                }
            }

            if (retineCoef.equals(""))
                retineCoef = "1";
            if (retineCoef.equals("-"))
                retineCoef = "-1";
            if (retineGrad.equals("")) {
                if (ok2 == 1) throw new Exception("dupa '^' nu este scris niciun nr/cifra!!!");
                if (ok == 1)
                    retineGrad = "1";
                else
                    retineGrad = "0";
            }
            creareCoef = Double.parseDouble(retineCoef);
            creareGrad = Integer.parseInt(retineGrad);
            polinomRezultat.getSirMonoame().add(new Monom(creareGrad, creareCoef)); //am adaugat monomul in polRez si se va trece la urm linia, adica se va intoarce in for si daca nu s-a terminat stringul, il va adauga si pe urm monom

            if (i < polinom.length() && polinom.charAt(i) == '-') {
                i--;
            }
        }
        return polinomRezultat;
    }

}
