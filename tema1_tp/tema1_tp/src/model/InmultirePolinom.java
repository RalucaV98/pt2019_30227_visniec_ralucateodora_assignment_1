package model;

/**
 * Class InmultirePolinom
 */
public class InmultirePolinom {

    /**
     * Calculeaza inmultirea a doua polinoame
     * Rezultatul este un polinom
     *
     * @param p1
     * @param p2
     * @return
     */
    public Polinom inmultire(Polinom p1, Polinom p2) {
        Polinom rezultat = new Polinom();
        for (Monom i : p1.getSirMonoame()) {
            for (Monom j : p2.getSirMonoame()) {
                Monom m = new Monom(i.getGrad() + j.getGrad(), j.getCoeficient() * i.getCoeficient());
                rezultat.getSirMonoame().add(m);
            }
        }
        rezultat.verifica();
        return rezultat;
    }

}
