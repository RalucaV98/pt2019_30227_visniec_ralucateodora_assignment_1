package model;

/**
 * Class ScaderePolinom
 */
public class ScaderePolinom {

	/**
	 * Efectureaza operatia de scadere dintre 2 polinoame.
	 * @param p1
	 * @param p2
	 * @return
	 */
	public Polinom scadere(Polinom p1, Polinom p2) {
			AdunarePolinom operatie = new AdunarePolinom();
			for(Monom m: p2.getSirMonoame()) {
				m.setCoeficient(-1*m.getCoeficient());
			}
			return operatie.adunare(p1, p2);
	}

}
