package model;

/**
 * class DerivarePolinom

 */
public class DerivarePolinom {
	 /** Se ocupa de derivarea unui polinom
	   * Rezultat este un polinom
	  */
	public Polinom derivare(Polinom polinom) {
		Polinom rezultat=new Polinom();

		for(Monom i : polinom.getSirMonoame()) {
			if(i.getGrad()!=0)
			{
				Monom monom = new Monom(i.getGrad()-1,i.getCoeficient()*i.getGrad());
				rezultat.getSirMonoame().add(monom);
			}
		}

		return rezultat;
	}
}
