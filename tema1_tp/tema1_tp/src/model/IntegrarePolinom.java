package model;

public class IntegrarePolinom {

	Polinom rezultat = new Polinom();

	public Polinom integrare(Polinom polinom) {
		for (Monom i : polinom.getSirMonoame()) {
			if((i.getGrad()+1)!=0) {
				Monom monom = new Monom(i.getGrad() + 1, i.getCoeficient() / (i.getGrad() + 1));
				rezultat.getSirMonoame().add(monom);

		}
		
	}
		return rezultat;
}

}
