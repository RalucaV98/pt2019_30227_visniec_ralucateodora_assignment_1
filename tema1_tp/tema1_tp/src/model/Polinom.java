package model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * class Polinom
 */
public class Polinom {
    private List<Monom> sirMonoame;

    public Polinom() {
        sirMonoame = new ArrayList<>();
    }


    public List<Monom> getSirMonoame() {
        return sirMonoame;
    }

    public void setSirMonoame(List<Monom> sirMonoame) {
        this.sirMonoame = sirMonoame;
    }


    /**
     * Verificam daca exista un monom cu gradul "grad" in polinomul curent / pe care se apeleaza functia
     */
    public boolean contineGrad(int grad) {
        for (Monom monom : this.getSirMonoame()) {
            if (monom.getGrad() == grad) {
                return true;
            }
        }
        return false;
    }

    public String afisare() {
        String afisareEcran = new String();
        this.verifica();
        this.ordonareDupaGrad();
        this.stergeZero(); // pentru a nu aparea semn inainte
        for (Monom i : sirMonoame) {
            {
                if (i.equals(sirMonoame.get(sirMonoame.size() - 1))) {
                    afisareEcran += i.toString(); //afisez cate un monom si fac concatenarea tuturor monoamelor,ulterior pt a iesi un polinom
                } else {
                    afisareEcran += i.toString() + "+";
                }
            }

        }

        if (sirMonoame.isEmpty())
            afisareEcran += '0';
        int i;
        for (i = 0; i < afisareEcran.length(); i++) {
            //polinom[i] - pt a folosi var asta, tb convertit intai intr un sir de caractere
            if ((afisareEcran.charAt(i) == '+' || afisareEcran.charAt(i) == '-') && (afisareEcran.charAt(i + 1) == '+' || afisareEcran.charAt(i + 1) == '-')) //fct pt cele4 cazuri
            {
                switch (afisareEcran.charAt(i)) {
                    case '+':
                        switch (afisareEcran.charAt(i + 1)) {
                            case '+':
                                afisareEcran = afisareEcran.replace("++", "+");
                                break;

                            case '-':
                                afisareEcran = afisareEcran.replace("+-", "-");
                                break;

                        }

                    case '-':
                        switch (afisareEcran.charAt(i + 1)) {
                            case '+':
                                afisareEcran = afisareEcran.replace("-+", "-");
                                break;

                            case '-':
                                afisareEcran = afisareEcran.replace("--", "-");
                                break;
                        }

                }
            }
        }

        return afisareEcran;
    }


    /**
     * Combina monoamele cu grad egal adunand coeficienti
     */
    public void verifica() {
        List<Monom> rezultat = new ArrayList<>();

        boolean exista = true;

        for (Monom monoame : this.sirMonoame) {
            exista = false;
            for (Monom monomX : rezultat) {
                if (monoame.getGrad() == monomX.getGrad()) {
                    exista = true;
                    monomX.setCoeficient(monomX.getCoeficient() + monoame.getCoeficient());
                    break;
                }
            }

            if (!exista) {
                rezultat.add(monoame);
            }
        }

        this.sirMonoame = rezultat;
    }

    /**
     * Sterge monomoale a caror coeficient e zero
     */
    public void stergeZero() {
        List<Monom> toRemove = new ArrayList<>();
        for (Monom monoame : sirMonoame) {
            if (monoame.getCoeficient() == 0)
                toRemove.add(monoame);
        }
        sirMonoame.removeAll(toRemove);
    }

    /**
     * Sortare lista de monoame dupa grad
     */
    public void ordonareDupaGrad() {
        Collections.sort(sirMonoame);
    }

    @Override
    public boolean equals(Object obj) {
        Polinom p = (Polinom) obj;

        p.verifica();
        p.stergeZero();
        p.ordonareDupaGrad();

        this.verifica();
        this.stergeZero();
        this.ordonareDupaGrad();


        if (p.getSirMonoame().size() != this.sirMonoame.size()) {
            return false;
        }

        boolean returnValue = true;

        for (int i = 0; i < this.sirMonoame.size(); i++) {
            returnValue &= (this.sirMonoame.get(i).getGrad() == p.sirMonoame.get(i).getGrad());
            returnValue &= (this.sirMonoame.get(i).getCoeficient() == p.sirMonoame.get(i).getCoeficient());
        }

        return returnValue;
    }
}
