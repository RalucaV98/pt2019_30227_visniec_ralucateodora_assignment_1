package control;

import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import view.View;
import model.*;

public class ControlButoane implements ActionListener {
    private View g;
    private Parsare parsare = new Parsare();

    public ControlButoane(View g) {
        this.g = g;
    }

    /**
     * Action listener pentru butonul Calculazeaza al view-ului
     * In functie de selectia facuta se calculeaza urmatoarele operatii
     * adunare, scadere, inmultire, integreare, derivare si impartire
     * <p>
     * Rezultatul este scris intr-un textField
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        switch (g.getSelectare().getSelectedIndex()) {
            //
            case 0:
                try {
                    Polinom p1 = parsare.parsare(g.getScriePolinom1().getText()); // stringul din text field
                    p1.verifica();
                    Polinom p2 = parsare.parsare(g.getScriePolinom2().getText());
                    p2.verifica();
                    Polinom rezultat1;
                    AdunarePolinom operatieAdunare = new AdunarePolinom();
                    rezultat1 = operatieAdunare.adunare(p1, p2);
                    g.getScrieRezultat().setText(rezultat1.afisare()); // transforma polinomul intr un string
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(g.getFrame(), "Format gresit");
                }
                break;
            case 1:
                try {
                    Polinom po1 = parsare.parsare(g.getScriePolinom1().getText());
                    po1.verifica();
                    Polinom po2 = parsare.parsare(g.getScriePolinom2().getText());
                    po2.verifica();
                    Polinom rezultat2;
                    ScaderePolinom operatieScadere = new ScaderePolinom();
                    rezultat2 = operatieScadere.scadere(po1, po2);
                    g.getScrieRezultat().setText(rezultat2.afisare());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(g.getFrame(), "Format gresit");
                }

                break;

            case 2:
                try {
                    Polinom pol1 = parsare.parsare(g.getScriePolinom1().getText());
                    Polinom pol2 = parsare.parsare(g.getScriePolinom2().getText());
                    pol1.verifica();
                    pol2.verifica();
                    Polinom rezultat3;
                    InmultirePolinom operatieInmultire = new InmultirePolinom();
                    rezultat3 = operatieInmultire.inmultire(pol1, pol2);
                    g.getScrieRezultat().setText(rezultat3.afisare());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(g.getFrame(), "Format gresit");
                }

                break;

            case 3:
                try {
                    Polinom poli1 = parsare.parsare(g.getScriePolinom1().getText());
                    Polinom poli2 = parsare.parsare(g.getScriePolinom2().getText());
                    ImpartirePolinom impartirePolinom = new ImpartirePolinom();

                    Polinom[] rezultat = impartirePolinom.impartire(poli1, poli2);
                    g.getScrieRezultat().setText("Rezultat " + rezultat[0].afisare() + " rest: " + rezultat[1].afisare());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(g.getFrame(), "Format gresit");

                }
                break;

            case 4:
                try {
                    Polinom polinom = parsare.parsare(g.getScriePolinom1().getText());
                    polinom.verifica();
                    Polinom rezultat5;
                    DerivarePolinom operatieDerivare = new DerivarePolinom();
                    rezultat5 = operatieDerivare.derivare(polinom);
                    g.getScrieRezultat().setText(rezultat5.afisare());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(g.getFrame(), "Format gresit");
                }
                ;

                break;

            case 5:
                try {
                    Polinom polinom1 = parsare.parsare(g.getScriePolinom1().getText());
                    polinom1.verifica();
                    Polinom rezultat6;
                    IntegrarePolinom operatieIntegrare = new IntegrarePolinom();
                    rezultat6 = operatieIntegrare.integrare(polinom1);
                    g.getScrieRezultat().setText(rezultat6.afisare());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(g.getFrame(), "Format gresit");
                }

                break;
        }

    }
}